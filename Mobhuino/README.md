# libMobhuino

[![Open Source](https://badges.frapsoft.com/os/v2/open-source.png?v=103)](https://github.com/ellerbrock/open-source-badges/)
[![License: GPL3](https://img.shields.io/badge/License-GPL3-green.svg)](https://opensource.org/licenses/GPL-3.0)

_Uma **biblioteca** que permite programar em linguagem **Arduino** utilizando comandos facilitados em **PT-BR** baseada no [Brasilino](https://github.com/OtacilioN/Brasilino)._

 Veja como é fácil usar nesse exemplo de piscar uma LED:

```c++
#include <Mobhuino.h>

funcao configurar() {
    saida(13);
}

funcao repetir() {
    ligar(13);
    esperar(1);
    desligar(13);
    esperar(1);
}
```

A libMobhuino é uma **biblioteca aberta**

## Documentação

[1. Como utilizar:](/INSTALACAO.md)

- Instalação através do gerenciador de biblioteca
- Instalação manual (Alternativa)
- Exemplos

[2. Tabela de instruções:](/TABELA_DE_INSTRUCOES.md)

- Estrutura Geral
- Estruturas de Controle
- Tipos de Dados Comuns
- Tipos de Dados Lógicos
- Funções

## Artigos Relacionados

"Programacao do Mobhuino em portugues [Blog Gênio Cristão](https://geniocristao.com.br/programacao-do-mobhuino-em-portugues/)


## Licença

Brasilino está licenciado sob a licença GPL-3.0, e tem seus exemplos baseados no trabalho do projeto [Arduino](https://github.com/arduino/Arduino).
Veja a [licença](https://github.com/OtacilioN/Brasilino/blob/master/LICENSE) para mais informações.

Sinta-se livre para enviar um email para: hamilton@geniocristao.com.br
