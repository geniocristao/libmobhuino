/*
    SerialOlaMundo
    Escreve no serial monitor a escrita "Ola MUndo" infinitamente
    
    Criado em 07/2021 
    por Hamilton Sena - Projeto Gênio Cristão
    
*/    

#include <Mobhuino.h> // Inclue a biblioteca Mobhuino
#include <HIDSerial.h> // Inclue a biblioteca HIDSerial
 
HIDSerial serial; //Cria o objeto serial para ser utilizado

funcao configurar(){
  
  mbIniciarSerial(9600); //Inicializa a Serial com a velocidade de 9600
 
}
 
funcao repetir(){ 
 
  mbEscreverSerialn("Ola Mundo"); //Imprime no Serial Monitor
  mbManterConexaoSerial(); //Mantém a conexão Serial
  esperar(0,5);
 
}


