/*
    SerialLed
    Liga o LED do pino 13 qunado é eviado a letra L e desligado quando é
    enviado a lestra D
    
    Criado em 07/2021 
    por Hamilton Sena - Projeto Gênio Cristão
    
*/    


#include <Mobhuino.h> // Inclue a biblioteca Mobhuino
#include <HIDSerial.h> // Inclue a biblioteca HIDSerial
 
HIDSerial serial; //Cria o objeto serial para ser utilizado
 
//variavel que guarda o dado vindo da serial
positivo caractere leitura[1]; //Se L Liga se D Desliga
 
//define o pino onde o LED estão conectados
constante inteiro led = 13; 
 
funcao configurar() {   
 
  mbIniciarSerial(); //Inicializa a Serial com a velocidade de 9600      
 
  //Configura o pino do LED como saída   
  saida(led);   
 
  //Mantém os LEDs desligados assim que iniciar o programa    
  escreverDigital (led, BAIXO);    
  
}
 
funcao repetir() {   
  
  //Verifica se há algum dado no conector serial   
  enquanto (mbSerialRecebido()) {     //serial.available
    
    //Lê o dado vindo da Serial e armazena na variável leitura     
    mbLerSerial(leitura); 
 
    //Se a variável leitura for igual a 'l' ou 'L' liga o led 
    se (leitura[0] == 'l' || leitura[0] =='L') {        
   
      escreverDigital(led,ALTO);
      mbEscreverSerialn("LED Ligado");     
 
    }
 
    //Se ela for igual a  'd' ou 'D' desliga o led     
    se (leitura[0] == 'd' || leitura[0] =='D') {
       
      escreverDigital(led,BAIXO);
      mbEscreverSerialn("LED Desligado");      
 
    }
 
 
  }
 
  mbManterConexaoSerial(); //Mantem a conexão Serial
 
}
