# libMobhuino

[![Open Source](https://badges.frapsoft.com/os/v2/open-source.png?v=103)](https://github.com/ellerbrock/open-source-badges/)
[![License: GPL3](https://img.shields.io/badge/License-GPL3-green.svg)](https://opensource.org/licenses/GPL-3.0)

_Uma **biblioteca** que permite programar em linguagem **Arduino** utilizando comandos facilitados em **PT-BR** baseada no [Brasilino](https://github.com/OtacilioN/Brasilino)._

 Veja como é fácil usar nesse exemplo de piscar uma LED:

```c++
#include <Mobhuino.h>

funcao configurar() {
    saida(13);
}

funcao repetir() {
    ligar(13);
    esperar(1);
    desligar(13);
    esperar(1);
}
```

A libMobhuino é uma **biblioteca aberta**

## Documentação

### 1. Como utilizar

 1.1 Instalação da biblioteca **Mobhuino**

- Primeiro baixe a biblioteca clicando [aqui](https://bitbucket.org/geniocristao/libmobhuino/downloads/libMobhuino_v1.zip) 
- Abra a sua IDE do Arduino, clique em ``Sketch > Incluir Biblioteca > Adicionar biblioteca .ZIP``
- Procure a biblioteca Mobhuino baixada em arquivo .ZIP em sua pasta de downloads
- Pronto, agora a sua biblioteca está instalada e pronta para ser utilizada!

 1.2 Instalação da biblioteca **HIDSerial**
 
- Primeiro baixe a biblioteca clicando [aqui](https://bitbucket.org/geniocristao/libmobhuino/downloads/HIDSerial.zip) 
- Abra a sua IDE do Arduino, clique em ``Sketch > Incluir Biblioteca > Adicionar biblioteca .ZIP``
- Procure a biblioteca HIDSerial baixada em arquivo .ZIP em sua pasta de downloads
- Pronto, agora a sua biblioteca está instalada e pronta para ser utilizada!

 1.3 Download do monitor serial

- Para a versão Windows x64 [aqui](https://bitbucket.org/geniocristao/libmobhuino/downloads/SerialMonitor_win64.zip) 
- Para a versão Windows x32 [aqui](https://bitbucket.org/geniocristao/libmobhuino/downloads/SerialMonitor_win32.zip) 
- Para a versão Linux x64 [aqui](https://bitbucket.org/geniocristao/libmobhuino/downloads/SerialMonitor_linux64.zip) 
- Para a versão Linux x32 [aqui](https://bitbucket.org/geniocristao/libmobhuino/downloads/SerialMonitor_linux32.zip) 
- Para a versão Macosx [aqui](https://bitbucket.org/geniocristao/libmobhuino/downloads/SerialMonitor_macosx.zip) 
 

## Artigos Relacionados

- "Programacao do Mobhuino em portugues" [Blog Gênio Cristão](https://geniocristao.com.br/programacao-do-mobhuino-em-portugues/)
- "Instalação do aplicativo e biblioteca de comunicação serial HIDSerial" [Blog Gênio Cristão](https://geniocristao.com.br/instalacao-do-aplicativo-e-biblioteca-de-comunicacao-serial-hidserial/)


## Licença

Brasilino está licenciado sob a licença GPL-3.0, e tem seus exemplos baseados no trabalho do projeto [Arduino](https://github.com/arduino/Arduino).
Veja a [licença](https://github.com/OtacilioN/Brasilino/blob/master/LICENSE) para mais informações.

Sinta-se livre para enviar um email para: hamilton@geniocristao.com.br
